package net.project_ile.weathermonitor;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONObject;

public class DisplayActivity extends FragmentActivity implements DownloadCallback {

    private boolean mDownloading = false;
    private NetworkFragment mNetworkFragment;
    String provinceName = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Create new view
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        // Get Intent
        Intent intent = getIntent();
        provinceName = intent.getStringExtra(MainActivity.DISPLAY_INFO_MESSAGE);
        // Get province name from intent
        String url = String.format("http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric&appid=b3311e4d3dd5a7f16dcbbf97018563aa", provinceName);
        System.out.println(url);
        mNetworkFragment = NetworkFragment.getInstance(this.getSupportFragmentManager(), url);
        SharedPreferences myPreference = this.getSharedPreferences(getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = myPreference.edit();
        editor.putString(getString(R.string.last_selected_province), provinceName);
        editor.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!mDownloading && mNetworkFragment != null) {
            // Execute the async download.
            mNetworkFragment.startDownload();
            mDownloading = true;
        }
    }

    @Override
    public void updateFromDownload(Object result) {
        if(result == null) {
            showDialog(this, "@string/local_error_title", "@string/load_error", "@string/load_error_ok");
            return;
        }
        try {
            TextView txtProvince = (TextView) findViewById(R.id.txtProvince);
            txtProvince.setText(provinceName);

            JSONObject reader = new JSONObject(result.toString());
            JSONObject main = reader.getJSONObject("main");

            TextView txtTemperature = (TextView) findViewById(R.id.txtTemperature);
            txtTemperature.setText(main.getString("temp") + " C");

            TextView txtPressure = (TextView) findViewById(R.id.txtPresurre);
            txtPressure.setText(main.getString("pressure") + " hPa");

            TextView txtHumidity = (TextView) findViewById(R.id.txtHumidity);
            txtHumidity.setText(main.getString("humidity") + " %");

            JSONObject wind = reader.getJSONObject("wind"); //select wind table from JSON

            TextView txtWind = (TextView) findViewById(R.id.txtWind);
            txtWind.setText("Speed: "+ wind.getString("speed")+ " mps/ Degree: "+ wind.getString("deg")+ "degrees");

        } catch (Exception ex) {
            showDialog(this, "@string/local_error_title", "@string/load_error", "@string/load_error_ok");
        }
    }

    @Override
    public NetworkInfo getActiveNetworkInfo() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo;
    }

    protected void showDialog(Context ct, String title, String message, String btnMessage)
    {
        new AlertDialog.Builder(ct)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton(btnMessage,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
    }
    @Override
    public void onProgressUpdate(int progressCode, int percentComplete) {
        ProgressBar progressBar;
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        switch(progressCode) {
            case Progress.ERROR:
                showDialog(this, "@string/local_error_title", "@string/load_error", "@string/load_error_ok");
                break;
            case Progress.CONNECT_SUCCESS:
                // TODO
                progressBar.setProgress(0);
                break;
            case Progress.GET_INPUT_STREAM_SUCCESS:
                // TODO
                break;
            case Progress.PROCESS_INPUT_STREAM_IN_PROGRESS:
                progressBar.setProgress(progressBar.getMax() / 100 * percentComplete);
                // TODO
                break;
            case Progress.PROCESS_INPUT_STREAM_SUCCESS:
                // TODO
                progressBar.setProgress(progressBar.getMax());
                break;
        }
    }

    @Override
    public void finishDownloading() {
        mDownloading = false;
        if (mNetworkFragment != null) {
            mNetworkFragment.cancelDownload();
        }
    }
}
